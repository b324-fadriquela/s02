<?php require_once "./code.php"; ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>S02: Selection Control Structures and Array Manipulation</title>
</head>
<body>
	
	<h3>Array Manipulation</h3>
		<?php insertToStudent("John Doe"); ?>
		<?php dispStudent(); ?>
		<?php getStudentCount(); ?>

		<?php insertToStudent("Timmy Bagel"); ?>
		<?php dispStudent(); ?>
		<?php getStudentCount(); ?>
		
		<?php removeFirstStudent(); ?>
		<?php dispStudent(); ?>
		<?php getStudentCount(); ?>
</body>
</html>